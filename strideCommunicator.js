const request = require('request');

function enviarMensaje(titulo, año, link) {
    var clientServerOptions = {
        uri: process.env.STRIDE_URL,
        body: generarMensaje(titulo, año, link),
        method: 'POST',
        auth: {
            'bearer': process.env.STRIDE_ACCESS_TOKEN
        },
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        if (response.statusCode != 201) {
          console.log(error, response.body);
        }
        return;
    });
}

// Mensaje con formato - https://developer.atlassian.com/cloud/stride/learning/formatting-messages/
function generarMensaje(titulo, año, link) {
    var mensaje = `{"body":
    {
        "version": 1,
        "type": "doc",
        "content": [
          {
            "type": "heading",
            "attrs": {
              "level": 3
            },
            "content": [
              {
                "type": "text",
                "text": "${process.env.PUBLICACION_TITULO}",
                "marks": [
                  {
                    "type": "strong"
                  }
                ]
              }
            ]
          },
          {
            "type": "panel",
            "attrs": {
              "panelType": "info"
            },
            "content": [
              {
                "type": "paragraph",
                "content": [
                  {
                    "type": "text",
                    "text": "${titulo} (${año})",
                    "marks": [
                      {
                        "type": "link",
                        "attrs": {
                          "href": "${link}"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    }`;
    return mensaje;
}

module.exports.enviarMensaje = enviarMensaje;
