const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const peliculasRepo = require('./peliculasRepository');

function Open(tareaProgramada) {
    app.set('view engine', 'ejs'); // establece el motor plantillas para vistas HTML
    app.use(express.static('public')); // permite a express acceder a la carpeta public para poder leer el fichero css
    app.use(bodyParser.urlencoded({ extended: true })); // permite a express acceder a bodyParser para leer datos del request

    // Servicio para consultar el estado del proceso
    app.get('/', (req, res) => {
        // res.send('Roboice en ejecución<br>');
        // TODO fecha debe estar en horario local
        var fecha = tareaProgramada.nextInvocation().toISOString();
        res.render('index', { fecha_siguiente: fecha });
    });

    // Servicio para mostrar el formulario de peliculas
    app.get('/peliculas', (req, res) => {
        peliculasRepo.getEstadisticas((contadores) => {
            res.render('peliculas', { mensaje: null, total: contadores.total, publicadas: contadores.publicadas, sinpublicar: contadores.sinpublicar });
        });
    });

    // Servicio para insertar una nueva pelicula
    app.post('/peliculas', (req, res) => {
        var pelicula = { descripcion: req.body.descripcion, año: req.body.año, codigo: req.body.codigo};

        peliculasRepo.addPelicula(pelicula, (nuevoId) => {
            var cadena = 'Película insertada con código: ' + nuevoId;

            peliculasRepo.getEstadisticas((contadores) => {
                res.render('peliculas', { mensaje: cadena, total: contadores.total, publicadas: contadores.publicadas, sinpublicar: contadores.sinpublicar });
            });
        });
    });

    // Comprobación de variables de entorno
    function envcheck() {
        if (!process.env.STRIDE_URL || !process.env.STRIDE_ACCESS_TOKEN || !process.env.NODE_ENV) {
            console.error('Please set STRIDE_URL, STRIDE_ACCESS_TOKEN and NODE_ENV as env variables in a .env or on the system.');
            process.exit(1);
        }
        if (!process.env.APP_NAME) process.env.APP_NAME = 'Roboice App';
        if (!process.env.PORT) process.env.PORT = 8080;
    }

    // Servidor de peticiones web
    app.listen(process.env.PORT, () => {
        envcheck();
        console.log(`Roboice escuchando en el puerto ${process.env.PORT}`)
    });
}

module.exports.Open = Open;
