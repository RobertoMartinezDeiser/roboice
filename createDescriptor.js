// Se ha optado por enviar mensajes directos a Stride con un token de autenticación
// no es necesario crear una App en Atlassian ni utilizar un descriptor

// module.exports = function(req) {
//     return {
//       baseUrl: `https://${req.headers.host}`,
//       key: 'roboiceApp',
//       lifecycle: {
//         installed: '/lifecycle/installed',
//         uninstalled: '/lifecycle/uninstalled',
//       },
//       modules: {
//         'chat:bot': [
//           {
//             key: 'hello-bot',
//             mention: {
//               url: '/webhooks/mention',
//             },
//             directMessage: {
//               url: '/webhooks/mention',
//             },
//           },
//         ],
//         'chat:bot:messages': [
//           {
//             key: 'hello-ping',
//             pattern: '.*hello.*',
//             url: '/webhooks/message',
//           },
//         ],
//       },
//     };
//   };
  