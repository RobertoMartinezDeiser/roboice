const schedule = require('node-schedule');
const sc = require('./strideCommunicator');
const peliculasRepo = require('./peliculasRepository');
const server = require('./server');
require('dotenv').config();

var tareaProgramada = schedule.scheduleJob(process.env.TASK_PROGRAMACION, () => {
    peliculasRepo.getSiguientePelicula((resultado) => {
        if (resultado != undefined) {
            console.log('Mensaje enviado con la película: ' + resultado.descripcion);
            if (process.env.ENVIAR_STRIDE == 'true') {
                sc.enviarMensaje(resultado.descripcion, resultado.año, `${process.env.TMDB_URL}${resultado.codigo}`);
            }
        } else {
            console.log('No hay películas para publicar. Se cancela la tarea programada');
            tareaProgramada.cancel();
        }
    });    
});

server.Open(tareaProgramada);

// Prueba de envío de mensaje
//sc.enviarMensaje('Ready Player One', '2018', 'https://www.themoviedb.org/movie/333339-ready-player-one');