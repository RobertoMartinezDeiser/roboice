# Roboice

Proyecto personal para publicar mensajes de forma programada en Stride

### Referencias

* Programador de tareas - https://github.com/node-schedule/node-schedule
* Integración con Stride - https://developer.atlassian.com/cloud/stride/
* Empezar con la primera aplicación - https://developer.atlassian.com/cloud/stride/getting-started/
* Enviar mensajes de forma sencilla a una room de Stride - https://developer.atlassian.com/cloud/stride/security/authentication/
* Construir apps para Stride - https://developer.atlassian.com/cloud/stride/integrating-with-stride/
* App descriptor - https://developer.atlassian.com/cloud/stride/blocks/app-descriptor/
* Stride API - https://developer.atlassian.com/cloud/stride/rest/
* Acceder a base de datos - https://github.com/mysqljs/mysql
* Dockerfile - https://docs.docker.com/engine/reference/builder/
* nodejs y docker - https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
* Winston - https://github.com/winstonjs/winston
* CallBack hell - http://callbackhell.com/
* Promises, control del flujo de ejecución - https://medium.com/dev-bits/writing-neat-asynchronous-node-js-code-with-promises-32ed3a4fd098
* themoviedb API - https://www.themoviedb.org/documentation/api

### Tareas

* DONE - Programar tareas
* DONE - Añadir descriptor
* DONE - Usar variables de configuración por entorno
* DONE - Autenticación para poder enviar mensajes a una room de Stride
* DONE - Generar token en una conversación de Stride
* NO NECESARIO - Crear la aplicación en Atlassian
* DONE - Enviar un mensaje de prueba
* NO ES POSIBLE - Enviar un mensaje con una imagen incrustada
* DONE - Leer información de una base de datos
* DONE - Añadir API para insertar nuevas películas en la tabla de BD
* DONE - Añadir formulario web para insertar nuevas películas
* DONE - Añadir API para consultar cuantas películas quedan por enviar
* DONE - Añadir formulario para consultar películas
* Mostrar la fecha de la siguiente ejecución de tarea programada en horario local
* Logger en fichero con winston
* Mejorar Callback hell con promises
* Crear un interface web para arrancar, parar y consultar tareas programadas
* Leer información de themoviedb