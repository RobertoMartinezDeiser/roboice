const mysql = require('mysql');

function crearConexion() {
    var connection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    });

    connection.connect(function(err) {
        if (err) {
            console.error('Error conectando a la base de datos: ' + err.stack);
            return;
        }
        //console.log('Conectado a la base de datos con id: ' + connection.threadId);
    });

    return connection;
}

function getSiguientePelicula(callback) {
    var connection = crearConexion();

    var idActualizar;

    connection.query('SELECT * FROM peliculas WHERE fecha_publicacion IS NULL ORDER BY id LIMIT 1', function (error, results, fields) {
        if (error) {
            console.error('Error en la consulta de peliculas: ' + error);
        }
        //console.log('Peliculas obtenidas: ' + results.length);
        var objeto;
        if (results.length > 0) {
            idActualizar = results[0].id;
            objeto = { 'id': results[0].id, 'descripcion': results[0].descripcion, 'codigo': results[0].codigo, 'año': results[0].año };
    
            if (idActualizar != undefined && process.env.ACTUALIZAR_FECHA_PUBLICACION == 'true') {
                connection.query('UPDATE peliculas SET fecha_publicacion=NOW() WHERE id=?', [idActualizar], function (error, results, fields) {
                    if (error) {
                        console.error('Error en la actualización de la fecha de publicación. id: ' + idActualizar);
                    }
                });
            }
        }

        connection.end();
        callback(objeto);
    });
}

function getEstadisticas(callback) {
    var connection = crearConexion();
    var cadenaSql = 'SELECT count(id) total, ';
    cadenaSql += 'sum(case when fecha_publicacion is null then 1 else 0 end) sinpublicar, ';
    cadenaSql += 'sum(case when fecha_publicacion is not null then 1 else 0 end) publicadas ';
    cadenaSql += 'FROM peliculas ';
    connection.query(cadenaSql, function (error, results, fields) {
        if (error) {
            console.error('Error en la consulta de estadisticas: ' + error);
        }

        var contadores;
        if (results.length > 0) {
            contadores = { total: results[0].total, sinpublicar: results[0].sinpublicar, publicadas: results[0].publicadas };
        }

        connection.end();
        callback(contadores);
    });
}

function addPelicula(pelicula, callback) {
    var connection = crearConexion();

    connection.query('INSERT INTO peliculas (descripcion, año, codigo, fecha_publicacion) VALUES (?,?,?,null)', [pelicula.descripcion, pelicula.año, pelicula.codigo], function(error, results, fields) {
        if (error) {
            console.error('Error al insertar una película: ' + pelicula);
        }

        connection.end();
        callback(results.insertId);
    });
}

module.exports.getSiguientePelicula = getSiguientePelicula;
module.exports.getEstadisticas = getEstadisticas;
module.exports.addPelicula = addPelicula;
